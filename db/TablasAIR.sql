-- --------------------------------------------------------
-- Host:                         10.10.20.14
-- Versión del servidor:         5.7.22 - MySQL Community Server (GPL)
-- SO del servidor:              Linux
-- HeidiSQL Versión:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para analytics
CREATE DATABASE IF NOT EXISTS `analytics` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `analytics`;

-- Volcando estructura para tabla analytics.man_company
CREATE TABLE IF NOT EXISTS `man_company` (
  `user_analytics` varchar(50) NOT NULL,
  `brand` varchar(100) DEFAULT NULL,
  `node_npc` int(1) DEFAULT '0',
  `node_ndc` int(1) DEFAULT '0',
  `node_nwa` int(1) DEFAULT '0',
  `node_npa` int(1) DEFAULT '0',
  `node_logistics` int(1) DEFAULT '0',
  `node_sac` int(1) DEFAULT '0',
  `node_npm` int(1) DEFAULT '0',
  `node_pos` int(1) DEFAULT '0',
  `node_wap` int(1) DEFAULT '0',
  `node_sms` int(1) DEFAULT '0',
  `node_sga` int(1) DEFAULT '0',
  `node_fid` int(1) DEFAULT '0',
  `node_tas` int(1) DEFAULT '0',
  `node_npw` int(1) DEFAULT '0',
  `node_crm` int(1) DEFAULT '0',
  `user_father` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`user_analytics`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tomar en cuenta que 0 = inactivo y 1 es activo';

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_person
CREATE TABLE IF NOT EXISTS `man_person` (
  `id_person` bigint(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) DEFAULT NULL,
  `id_department` int(10) DEFAULT NULL,
  `device_id` varchar(50) DEFAULT NULL,
  `token` varchar(500) DEFAULT NULL,
  `wp_endpoint` text,
  `wp_auth` varchar(200) DEFAULT NULL,
  `wp_secret` text,
  `macaddr` varchar(50) DEFAULT NULL,
  `macaddr_bluetooth` varchar(50) DEFAULT NULL,
  `device_type` varchar(50) DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `lastname` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cedula` varchar(20) DEFAULT NULL,
  `phone_country` varchar(3) DEFAULT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `phone_home_country` varchar(3) DEFAULT NULL,
  `phone_home` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `email_confirmed` varchar(2) DEFAULT NULL,
  `image_profile` varchar(200) DEFAULT NULL,
  `facebook_id` varchar(20) DEFAULT NULL,
  `facebook_picture` varchar(200) DEFAULT NULL,
  `instagram_id` varchar(20) DEFAULT NULL,
  `instagram_picture` varchar(200) DEFAULT NULL,
  `twitter_id` varchar(20) DEFAULT NULL,
  `webchat_id` varchar(30) DEFAULT NULL,
  `telegram_id` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `pin` varchar(6) DEFAULT NULL,
  `publicity` varchar(1) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `genre` varchar(10) DEFAULT NULL,
  `qualification` double DEFAULT '0',
  `field_1` varchar(100) DEFAULT NULL,
  `field_2` varchar(100) DEFAULT NULL,
  `field_3` varchar(100) DEFAULT NULL,
  `field_4` varchar(100) DEFAULT NULL,
  `field_5` varchar(100) DEFAULT NULL,
  `field_6` varchar(100) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `gps_type` varchar(10) DEFAULT NULL,
  `gps_imei` varchar(50) DEFAULT NULL,
  `registered_in` varchar(20) DEFAULT 'App',
  `registered_by` varchar(20) DEFAULT NULL,
  `registered_location` varchar(50) DEFAULT NULL,
  `date_registration` datetime DEFAULT NULL,
  `date_lastseen` datetime DEFAULT NULL,
  PRIMARY KEY (`id_person`),
  KEY `user_macaddr` (`user`,`macaddr`),
  KEY `email_pin` (`email`,`pin`),
  KEY `FK_man_person_man_departments` (`id_department`),
  KEY `gps_imei` (`gps_imei`),
  KEY `cedula` (`cedula`),
  KEY `user_device_id_registered_in` (`user`,`device_id`,`registered_in`),
  KEY `id_person` (`id_person`),
  CONSTRAINT `FK_man_person_man_departments` FOREIGN KEY (`id_department`) REFERENCES `man_departments` (`id_department`),
  CONSTRAINT `FK_person_man_company` FOREIGN KEY (`user`) REFERENCES `man_company` (`user_analytics`)
) ENGINE=InnoDB AUTO_INCREMENT=491528 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_product
CREATE TABLE IF NOT EXISTS `man_product` (
  `id_product` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `id_category` int(10) NOT NULL,
  `id_sucbategory` int(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `detail` text,
  `reference` varchar(30) DEFAULT '0',
  `price` decimal(7,2) DEFAULT '0.00',
  `currency` varchar(10) DEFAULT NULL,
  `discount` decimal(2,2) DEFAULT '0.00',
  `qr_code` varchar(15) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `image_1` varchar(200) DEFAULT NULL,
  `image_2` varchar(200) DEFAULT NULL,
  `image_3` varchar(200) DEFAULT NULL,
  `isnew` varchar(1) DEFAULT '0',
  `ispromotion` varchar(1) DEFAULT '0',
  `visits` int(6) DEFAULT '0',
  `stock` int(6) DEFAULT '0',
  `status` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id_product`),
  KEY `FK_man_product_man_product_category` (`id_category`),
  KEY `FK_man_product_man_company` (`username`),
  KEY `FK_man_product_man_product_currency` (`username`,`currency`),
  KEY `FK_man_product_man_product_category_2` (`id_sucbategory`),
  CONSTRAINT `FK_man_product_man_company` FOREIGN KEY (`username`) REFERENCES `man_company` (`user_analytics`),
  CONSTRAINT `FK_man_product_man_product_category` FOREIGN KEY (`id_category`) REFERENCES `man_product_category` (`id_category`),
  CONSTRAINT `FK_man_product_man_product_category_2` FOREIGN KEY (`id_sucbategory`) REFERENCES `man_product_category` (`id_category`),
  CONSTRAINT `FK_man_product_man_product_currency` FOREIGN KEY (`username`, `currency`) REFERENCES `man_product_currency` (`user`, `name_currency`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_product_category
CREATE TABLE IF NOT EXISTS `man_product_category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `nivel` int(1) DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `external_link` varchar(200) DEFAULT NULL,
  `id_category_father` int(10) DEFAULT NULL,
  `order_menu` int(10) DEFAULT NULL,
  `visits` int(10) DEFAULT NULL,
  `status` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id_category`),
  KEY `FK_man_product_category_man_company` (`username`),
  CONSTRAINT `FK_man_product_category_man_company` FOREIGN KEY (`username`) REFERENCES `man_company` (`user_analytics`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_product_currency
CREATE TABLE IF NOT EXISTS `man_product_currency` (
  `user` varchar(50) NOT NULL,
  `name_currency` varchar(10) NOT NULL,
  `description_currency` varchar(50) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`user`,`name_currency`),
  CONSTRAINT `FK_man_product_currency_man_company` FOREIGN KEY (`user`) REFERENCES `man_company` (`user_analytics`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_rewards
CREATE TABLE IF NOT EXISTS `man_rewards` (
  `id_rewards` bigint(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `valid_from` datetime DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL,
  `purchase_minimum_value` decimal(6,2) DEFAULT NULL,
  `purchase_currency` varchar(10) DEFAULT NULL,
  `purchase_from` varchar(10) DEFAULT NULL,
  `value_win` decimal(6,2) DEFAULT NULL,
  `value_type` varchar(15) DEFAULT NULL COMMENT 'Value type = Currency, Porcentage',
  `reward_type` varchar(15) DEFAULT NULL,
  `status` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id_rewards`),
  KEY `FK_man_person_rewards_man_company` (`user`),
  KEY `FK_man_person_rewards_man_product_currency` (`user`,`purchase_currency`),
  CONSTRAINT `FK_man_person_rewards_man_company` FOREIGN KEY (`user`) REFERENCES `man_company` (`user_analytics`),
  CONSTRAINT `FK_man_person_rewards_man_product_currency` FOREIGN KEY (`user`, `purchase_currency`) REFERENCES `man_product_currency` (`user`, `name_currency`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_rewards_link
CREATE TABLE IF NOT EXISTS `man_rewards_link` (
  `id_link` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) DEFAULT NULL,
  `id_person` bigint(11) DEFAULT NULL,
  `key` varchar(10) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `payment_value` float(4,2) DEFAULT NULL,
  `payment_iva` float(4,2) DEFAULT NULL,
  `payment_method` varchar(50) DEFAULT NULL,
  `expiration` int(2) DEFAULT NULL,
  `status_link` varchar(50) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_link`),
  KEY `FK_man_rewards_link_man_company` (`user`),
  KEY `FK_man_rewards_link_man_person` (`id_person`),
  CONSTRAINT `FK_man_rewards_link_man_company` FOREIGN KEY (`user`) REFERENCES `man_company` (`user_analytics`),
  CONSTRAINT `FK_man_rewards_link_man_person` FOREIGN KEY (`id_person`) REFERENCES `man_person` (`id_person`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_rewards_payment_method
CREATE TABLE IF NOT EXISTS `man_rewards_payment_method` (
  `id_method` int(10) NOT NULL AUTO_INCREMENT,
  `method` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_method`),
  UNIQUE KEY `method` (`method`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_rewards_payment_user
CREATE TABLE IF NOT EXISTS `man_rewards_payment_user` (
  `id_method` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_method`,`username`),
  KEY `FK__man_company` (`username`),
  KEY `id_method_username` (`id_method`,`username`),
  CONSTRAINT `FK__man_company` FOREIGN KEY (`username`) REFERENCES `man_company` (`user_analytics`),
  CONSTRAINT `FK_man_rewards_payment_user_man_rewards_payment_method` FOREIGN KEY (`id_method`) REFERENCES `man_rewards_payment_method` (`id_method`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_rewards_purchase
CREATE TABLE IF NOT EXISTS `man_rewards_purchase` (
  `id_purchase` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) CHARACTER SET latin1 NOT NULL,
  `id_person` bigint(11) NOT NULL DEFAULT '0',
  `location` int(10) unsigned DEFAULT NULL,
  `invoice` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `description` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `value` decimal(6,2) NOT NULL DEFAULT '0.00',
  `iva` decimal(6,2) NOT NULL DEFAULT '0.00',
  `value_zero` decimal(6,2) NOT NULL DEFAULT '0.00',
  `value_delivery` decimal(6,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `purchase_from` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `address` text CHARACTER SET latin1,
  `latitude` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `longitude` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `delivery_main_street` varchar(200) DEFAULT NULL,
  `delivery_sencondary_street` varchar(200) DEFAULT NULL,
  `delivery_reference` varchar(200) DEFAULT NULL,
  `delivery_zone` varchar(200) DEFAULT NULL,
  `delivery_province` varchar(200) DEFAULT NULL,
  `delivery_city` varchar(200) DEFAULT NULL,
  `status_order` int(1) DEFAULT '0' COMMENT '0= ingresado, 1= despachado, 2= cancelado',
  `payment_method` varchar(50) DEFAULT NULL,
  `payment_reference` varchar(50) DEFAULT NULL,
  `payment_link` varchar(50) DEFAULT NULL,
  `payment_requestId` varchar(50) DEFAULT NULL,
  `payment_processUrl` varchar(100) DEFAULT NULL,
  `payment_expiration` datetime DEFAULT NULL,
  `payment_ipAddress` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_update_datetime` datetime DEFAULT NULL,
  `payment_reverse_status` varchar(20) DEFAULT NULL,
  `payment_reverse_datetime` datetime DEFAULT NULL,
  `id_chat` bigint(10) DEFAULT NULL,
  `status` bigint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_purchase`),
  UNIQUE KEY `user_invoice` (`user`,`invoice`),
  UNIQUE KEY `payment_reference` (`payment_reference`),
  UNIQUE KEY `payment_link` (`payment_link`),
  KEY `FK_man_person_purchases_man_person` (`id_person`),
  KEY `FK_man_person_purchases_man_location` (`location`),
  KEY `FK_man_person_purchases_man_product_currency` (`user`,`currency`),
  KEY `FK_man_rewards_purchase_box_chat` (`id_chat`),
  CONSTRAINT `FK_man_person_purchases_man_product_currency` FOREIGN KEY (`user`, `currency`) REFERENCES `man_product_currency` (`user`, `name_currency`),
  CONSTRAINT `FK_man_rewards_purchase_box_chat` FOREIGN KEY (`id_chat`) REFERENCES `box_chat` (`id_chat`),
  CONSTRAINT `FK_man_rewards_purchase_man_person` FOREIGN KEY (`id_person`) REFERENCES `man_person` (`id_person`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_rewards_redemption
CREATE TABLE IF NOT EXISTS `man_rewards_redemption` (
  `id_redemption` int(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `id_coupon` int(11) DEFAULT NULL,
  `id_person` bigint(10) DEFAULT NULL,
  `date_redemption` datetime DEFAULT NULL,
  PRIMARY KEY (`id_redemption`),
  KEY `FK_man_rewards_redemption_man_company` (`user`),
  KEY `FK_man_rewards_redemption_app_places` (`id_coupon`),
  KEY `FK_man_rewards_redemption_man_person` (`id_person`),
  CONSTRAINT `FK_man_rewards_redemption_app_places` FOREIGN KEY (`id_coupon`) REFERENCES `app_places` (`id`),
  CONSTRAINT `FK_man_rewards_redemption_man_company` FOREIGN KEY (`user`) REFERENCES `man_company` (`user_analytics`),
  CONSTRAINT `FK_man_rewards_redemption_man_person` FOREIGN KEY (`id_person`) REFERENCES `man_person` (`id_person`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla analytics.man_rewards_won
CREATE TABLE IF NOT EXISTS `man_rewards_won` (
  `id_won` bigint(10) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) DEFAULT NULL,
  `id_reward` bigint(10) DEFAULT NULL,
  `id_person` bigint(10) DEFAULT NULL,
  `value_won` decimal(6,2) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `date_won` datetime DEFAULT NULL,
  PRIMARY KEY (`id_won`),
  KEY `FK_man_person_rewards_won_man_product_currency` (`user`,`currency`),
  KEY `FK_man_person_rewards_won_man_person` (`id_person`),
  KEY `FK_man_person_rewards_won_man_person_rewards` (`id_reward`),
  CONSTRAINT `FK_man_person_rewards_won_man_person` FOREIGN KEY (`id_person`) REFERENCES `man_person` (`id_person`),
  CONSTRAINT `FK_man_person_rewards_won_man_person_rewards` FOREIGN KEY (`id_reward`) REFERENCES `man_rewards` (`id_rewards`),
  CONSTRAINT `FK_man_person_rewards_won_man_product_currency` FOREIGN KEY (`user`, `currency`) REFERENCES `man_product_currency` (`user`, `name_currency`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
