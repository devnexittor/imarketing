import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaingsComponent } from './campaings.component';
import { RouterModule } from '@angular/router';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {HttpClientModule} from '@angular/common/http';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {UploadModule} from '../../shared/upload/upload.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

@NgModule({
  declarations: [CampaingsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: CampaingsComponent
      }
    ]),
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    HttpClientModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    UploadModule,
    MatTooltipModule,
    MatSlideToggleModule
  ]
})
export class CampaingsModule { }
