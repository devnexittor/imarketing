import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterPagesComponent } from './footer-pages.component';
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [FooterPagesComponent],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  exports: [FooterPagesComponent]
})
export class FooterPagesModule { }
