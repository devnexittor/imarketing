import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BtnUploadComponent } from './btn-upload.component';

@NgModule({
  declarations: [BtnUploadComponent],
  imports: [
    CommonModule
  ],
  exports: [BtnUploadComponent]
})
export class BtnUploadModule { }
