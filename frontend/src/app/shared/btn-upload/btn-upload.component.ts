import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-btn-upload',
  templateUrl: './btn-upload.component.html',
  styleUrls: ['./btn-upload.component.scss']
})
export class BtnUploadComponent implements OnInit {

  indicatorButton = '.csv .png .jpeg . jpg';
  @Input() set indicator(value: string) {
    if (value) {
      this.indicatorButton = value;
    } else {
      this.indicatorButton = '.csv .png .jpeg . jpg';
    }
  }
  constructor() { }

  ngOnInit(): void {
  }

}
