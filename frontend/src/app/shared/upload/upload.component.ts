import { Component, OnInit } from '@angular/core';
import {ServiceService} from '../../service.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  files: File[] = [];
  showBtn       = false;
  indicator     = '';
  progressBar   = 0;

  constructor(
    private readonly serviceService: ServiceService
  ) { }

  ngOnInit(): void {
  }

  onSelect(event): void {
    this.showBtn  = true;
    let numImg    = 0;
    let numCsv    = 0;
    event.addedFiles.filter((file: File) => {
      console.log(file.type);
      if (file.type.includes('image')) {
        numImg += 1;
      }
      if (file.type.includes('excel')) {
        numCsv += 1;
      }
    });
    this.files.push(...event.addedFiles);
    this.indicator = `${ numCsv } .csv y ${ numImg } imágenes`;
  }

  // tslint:disable-next-line:typedef
  async processFiles() {
    this.showBtn = false;
    const pushProgress = (100 / this.files.length);
    for (const file of this.files) {
      this.indicator = `${file.name.charAt(0) + file.name.charAt(1)}...${file.name.substring((file.name.length - 15))}`;
      console.log(this.indicator);
      this.progressBar += pushProgress;
      await this.serviceService.uploadFiles();
    }
    // reseteamos las variables
    this.files        = [];
    this.progressBar  = 0;
    this.indicator    = '';
    console.log('se terminó');
  }

}
