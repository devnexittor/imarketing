import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadComponent } from './upload.component';
import { BtnUploadModule } from '../btn-upload/btn-upload.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [UploadComponent],
  imports: [
    CommonModule,
    BtnUploadModule,
    MatProgressBarModule,
    NgxDropzoneModule,
    MatButtonModule
  ],
  exports: [UploadComponent]
})
export class UploadModule { }
