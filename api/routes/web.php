<?php

use App\Http\Controllers\SimilarityMatrix;
use Illuminate\Support\Facades\Route;

Route::get('/', [SimilarityMatrix::class, 'GoSimilarityMatrix']);
