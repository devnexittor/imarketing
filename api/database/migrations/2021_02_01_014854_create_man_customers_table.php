<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_customers', function (Blueprint $table) {
            $table->id('id_customer');

            $table->unsignedBigInteger('user_analytics');
            $table->foreign('user_analytics')->references('id_company')->on('man_companies');

            $table->unsignedBigInteger('fk_id_person');
            $table->foreign('fk_id_person')->references('id_person')->on('man_person');

            $table->date('date_joined');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_customers');
    }
}
