<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIaCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ia_campaigns', function (Blueprint $table) {
            $table->id('id_campaign');
            $table->string('customer_email');
            $table->longText('html_campaign');
            $table->boolean('status');

            $table->unsignedBigInteger('fk_id_customer');
            $table->foreign('fk_id_customer')->references('id_customer')->on('man_customers');

            $table->unsignedBigInteger('fk_id_product');
            $table->foreign('fk_id_product')->references('id_product')->on('man_products');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ia_campaigns');
    }
}
