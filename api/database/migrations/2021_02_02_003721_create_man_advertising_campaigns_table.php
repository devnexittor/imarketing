<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManAdvertisingCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_advertising_campaigns', function (Blueprint $table) {
            $table->id('id_campaign');
            $table->unsignedBigInteger('user_analytics');
            $table->foreign('user_analytics')->references('id_company')->on('man_companies');
            /*$table->string('title_campaign');
            $table->string('description_campaign')->nullable();
            $table->float('price_product_discount')->nullable();
            $table->string('link_app_sale')->nullable();

            $table->boolean('channel_facebook')->nullable();
            $table->boolean('channel_instagram')->nullable();
            $table->boolean('channel_whatsapp')->nullable();
            $table->boolean('channel_twitter')->nullable();
            $table->boolean('channel_email')->nullable();*/

            $table->unsignedBigInteger('fk_id_person_published');
            $table->foreign('fk_id_person_published')->references('id_person')->on('man_person');

            $table->unsignedBigInteger('fk_id_product');
            $table->foreign('fk_id_product')->references('id_product')->on('man_products');

            /*$table->integer('filter_age_min')->nullable();
            $table->integer('filter_age_max')->nullable();

            $table->enum('filter_genre', ['M', 'F', 'A'])->default('A');

            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();

            $table->boolean('status')->default(true);*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_advertising_campaigns');
    }
}
