<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_chats', function (Blueprint $table) {
            $table->id('id_chat');

            $table->unsignedBigInteger('fk_id_customer');
            $table->foreign('fk_id_customer')->references('id_customer')->on('man_customers');

            $table->unsignedBigInteger('fk_id_agent');
            $table->foreign('fk_id_agent')->references('id_person')->on('man_person');

            /*$table->string('channel'); // facebook - wsp - etc
            $table->string('type_message'); // comment - message
            $table->boolean('status')->default(true);
            $table->string('status_chat')->default('open'); // open - pending - close*/

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_chats');
    }
}
