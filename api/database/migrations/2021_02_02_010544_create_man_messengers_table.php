<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManMessengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_messengers', function (Blueprint $table) {
            $table->id('id_messenger');
            $table->unsignedBigInteger('fk_id_chat');
            $table->foreign('fk_id_chat')->references('id_chat')->on('man_chats');

           /* $table->string('type'); // text - image - audio - video - file
            $table->string('message');
            $table->enum('direction', ['i', 'o']);*/

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_messengers');
    }
}
