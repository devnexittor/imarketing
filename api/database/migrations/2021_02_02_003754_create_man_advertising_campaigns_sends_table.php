<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManAdvertisingCampaignsSendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_advertising_campaigns_sends', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('fk_id_campaign');
            $table->foreign('fk_id_campaign')->references('id_campaign')->on('man_advertising_campaigns');

            $table->unsignedBigInteger('fk_id_customer');
            $table->foreign('fk_id_customer')->references('id_customer')->on('man_customers');

            /*$table->boolean('channel_facebook')->nullable();
            $table->boolean('channel_instagram')->nullable();
            $table->boolean('channel_whatsapp')->nullable();
            $table->boolean('channel_twitter')->nullable();
            $table->boolean('channel_email')->nullable();*/

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_advertising_campaigns_sends');
    }
}
