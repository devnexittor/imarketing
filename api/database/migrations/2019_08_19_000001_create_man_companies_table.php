<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_companies', function (Blueprint $table) {
            $table->id('id_company');
            $table->string('user_analytics');
            $table->string('brand')->default(null);
            $table->string('node_logistics')->default('0');
            $table->string('node_sac')->default('0');
            $table->string('node_sms')->default('0');
            $table->string('status')->default('1');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_companies');
    }
}
