<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManPersonSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_person_sales', function (Blueprint $table) {
            $table->id('id_person_sale');

            $table->unsignedBigInteger('fk_id_customer');
            $table->foreign('fk_id_customer')->references('id_customer')->on('man_customers');

            $table->unsignedBigInteger('fk_id_product');
            $table->foreign('fk_id_product')->references('id_product')->on('man_products');

            $table->unsignedBigInteger('fk_id_person_rsp');
            $table->foreign('fk_id_person_rsp')->references('id_person')->on('man_person');

            $table->unsignedBigInteger('fk_id_pay_method');
            $table->foreign('fk_id_pay_method')->references('id_pay_method')->on('man_payment_methods');

           /* $table->unsignedFloat('total_invoice')->nullable(); // total a pagar
            $table->unsignedFloat('iva')->nullable();
            $table->string('image_invoice')->nullable(); // foto de la factura
            $table->enum('status_sale', ['finished', 'cancel', 'in_process']);
            $table->boolean('status')->default(true);*/

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_person_sales');
    }
}
