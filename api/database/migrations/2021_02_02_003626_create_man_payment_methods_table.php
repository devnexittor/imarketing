<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManPaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_payment_methods', function (Blueprint $table) {
            $table->id('id_pay_method');
            $table->unsignedBigInteger('user_analytics');
            $table->foreign('user_analytics')->references('id_company')->on('man_companies');
            $table->string('method');
            $table->string('description')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_payment_methods');
    }
}
