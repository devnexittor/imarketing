<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_person', function (Blueprint $table) {
            $table->id('id_person');
            $table->string('user')->nullable();
            $table->string('id_department')->nullable();
            $table->string('device_id')->nullable();
            $table->string('token')->nullable();
            $table->string('wp_endpoint')->nullable();
            $table->string('wp_auth')->nullable();
            $table->string('wp_secret')->nullable();
            $table->string('macaddr')->nullable();
            $table->string('macaddr_bluetooth')->nullable();
            $table->string('device_type')->nullable();
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('cedula')->nullable();
            $table->string('phone_country')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone_home_country')->nullable();
            $table->string('phone_home')->nullable();
            $table->string('address')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('email')->nullable();
            $table->string('email_confirmed')->nullable();
            $table->string('image_profile')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('facebook_picture')->nullable();
            $table->string('instagram_id')->nullable();
            $table->string('instagram_picture')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('webchat_id')->nullable();
            $table->string('telegram_id')->nullable();
            $table->string('password')->nullable();
            $table->string('pin')->nullable();
            $table->string('publicity')->nullable();
            $table->string('birthday')->nullable();
            $table->string('genre')->nullable();
            $table->string('qualification')->nullable();
            $table->string('registered_in')->default("App");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_person');
    }
}
