<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_product_categories', function (Blueprint $table) {
            $table->id('id_category');
            $table->unsignedBigInteger('user_analytics');
            $table->foreign('user_analytics')->references('id_company')->on('man_companies');

            $table->unsignedBigInteger('id_category_father')->nullable();
            $table->foreign('id_category_father')->references('id_category')->on('man_product_categories');

            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->integer('nivel')->default(1);
            $table->string('image')->nullable();
            $table->string('external_link')->nullable();
            $table->integer('order_menu')->nullable();
            $table->integer('visits')->nullable();
            $table->string('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_product_categories');
    }
}
