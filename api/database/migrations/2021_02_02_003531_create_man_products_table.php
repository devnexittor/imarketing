<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_products', function (Blueprint $table) {
            $table->id('id_product');
            $table->unsignedBigInteger('fk_id_category');
            $table->foreign('fk_id_category')->references('id_category')->on('man_product_categories');

            $table->unsignedBigInteger('fk_id_product_currency');
            $table->foreign('fk_id_product_currency')->references('id_product_currency')->on('man_product_currencies');

            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->text('detail')->nullable();
            $table->string('reference')->nullable();
            $table->string('price')->nullable();
            $table->string('discount')->nullable();
            $table->string('qr_code')->nullable();
            $table->string('image')->nullable();
            $table->string('image_1')->nullable();
            $table->string('image_2')->nullable();
            $table->string('image_3')->nullable();
            $table->string('isnew')->nullable();
            $table->string('ispromotion')->nullable();
            $table->string('visits')->nullable();
            $table->string('stock')->nullable();
            $table->boolean('status')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_products');
    }
}
