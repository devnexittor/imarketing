<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManPersonSaleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('man_person_sale_details', function (Blueprint $table) {
            $table->id('if_person_sale_detail');
            $table->unsignedBigInteger('fk_id_person_sale');
            $table->foreign('fk_id_person_sale')->references('id_person_sale')->on('man_person_sales');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('man_person_sale_details');
    }
}
