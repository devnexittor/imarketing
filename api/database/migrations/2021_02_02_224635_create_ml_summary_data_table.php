<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMlSummaryDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ml_summary_data', function (Blueprint $table) {
            $table->id('id_summary_data');

            $table->unsignedBigInteger('user_analytics');
            $table->foreign('user_analytics')->references('id_company')->on('man_companies');

            $table->unsignedBigInteger('fk_id_customer');
            $table->foreign('fk_id_customer')->references('id_customer')->on('man_customers');

            $table->unsignedBigInteger('fk_id_product');
            $table->foreign('fk_id_product')->references('id_product')->on('man_products');

            $table->boolean('acquired');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ml_summary_data');
    }
}
