<?php

namespace Database\Factories;

use App\Models\MlSummaryData;
use Illuminate\Database\Eloquent\Factories\Factory;

class MlSummaryDataFactory extends Factory
{
    protected $model = MlSummaryData::class;

    public function definition()
    {
        return [
            'user_analytics'    => $this->faker->numberBetween(1,10),
            'fk_id_customer'    => $this->faker->numberBetween(1,150),
            'fk_id_product'     => $this->faker->numberBetween(1,100),
            'acquired'          => $this->faker->numberBetween(0,1)
        ];
    }
}
