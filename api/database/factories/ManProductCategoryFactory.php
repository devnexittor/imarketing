<?php

namespace Database\Factories;

use App\Models\ManProductCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ManProductCategoryFactory extends Factory
{
    protected $model = ManProductCategory::class;

    public function definition()
    {
        return [
            'user_analytics'    => $this->faker->numberBetween(1,10),
            'name'              => $this->faker->colorName,
            'description'       => $this->faker->realText(),
            'nivel'             => $this->faker->numberBetween(1, 2),
            'image'             => $this->faker->imageUrl(),
            'external_link'     => $this->faker->url,
            'order_menu'        => $this->faker->numberBetween(1, 10),
            'visits'            => $this->faker->numberBetween(0, 100),
        ];
    }
}
