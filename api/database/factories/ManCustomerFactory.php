<?php

namespace Database\Factories;

use App\Models\ManCustomer;
use Illuminate\Database\Eloquent\Factories\Factory;

class ManCustomerFactory extends Factory
{
    protected $model = ManCustomer::class;

    public function definition()
    {
        return [
            'user_analytics'    => $this->faker->numberBetween(1,5),
            'fk_id_person'      => $this->faker->numberBetween(1, 100),
            'date_joined'       => $this->faker->date(),
        ];
    }
}
