<?php

namespace Database\Factories;

use App\Models\ManPerson;
use Illuminate\Database\Eloquent\Factories\Factory;

class ManPersonFactory extends Factory
{
    protected $model = ManPerson::class;

    public function definition()
    {
        return [
            'name'      => $this->faker->name,
            'lastname'  => $this->faker->lastName,
            'email'     => $this->faker->email,
        ];
    }
}
