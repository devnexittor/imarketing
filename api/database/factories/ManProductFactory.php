<?php

namespace Database\Factories;

use App\Models\ManProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

class ManProductFactory extends Factory
{
    protected $model = ManProduct::class;

    public function definition()
    {
        return [
            'fk_id_category'            => $this->faker->numberBetween(1,50),
            'fk_id_product_currency'    => $this->faker->numberBetween(1,2),
            'name'                      => $this->faker->colorName,
            'description'               => $this->faker->realText(),
            'detail'                    => $this->faker->realText(),
            'reference'                 => $this->faker->url,
            'price'                     => $this->faker->randomFloat(2,3, 80 ),
            'discount'                  => $this->faker->numberBetween(0, 10),
            'qr_code'                   => $this->faker->imageUrl(),
            'image'                     => $this->faker->imageUrl(),
            'image_1'                   => $this->faker->imageUrl(),
            'image_2'                   => $this->faker->imageUrl(),
            'image_3'                   => $this->faker->imageUrl(),
            'isnew'                     => $this->faker->boolean,
            'ispromotion'               => $this->faker->boolean,
            'visits'                    => $this->faker->numberBetween(1,90),
            'stock'                     => $this->faker->numberBetween(0,100),
        ];
    }
}
