<?php

namespace Database\Seeders;

use App\Models\ManCompany;
use App\Models\ManProductCategory;
use Illuminate\Database\Seeder;

class ManProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ManProductCategory::factory()->count(50)->create();
    }
}
