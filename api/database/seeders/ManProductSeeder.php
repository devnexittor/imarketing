<?php

namespace Database\Seeders;

use App\Models\ManProduct;
use Illuminate\Database\Seeder;

class ManProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ManProduct::factory()->count(100)->create();
    }
}
