<?php

namespace Database\Seeders;

use App\Models\ManPerson;
use Illuminate\Database\Seeder;

class ManPersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ManPerson::factory()->count(100)->create();
    }
}
