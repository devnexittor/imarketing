<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('man_product_currencies')->insert([
            'name_currency' => 'USD',
        ]);
        DB::table('man_product_currencies')->insert([
            'name_currency' => 'PTS',
        ]);
        $this->call([
            ManPersonSeeder::class,
            ManCompanySeeder::class,
            ManCustomerSeeder::class,
            ManProductCategorySeeder::class,
            ManProductSeeder::class,
            MlSummaryDataSeeder::class,
        ]);
    }
}
