<?php

namespace Database\Seeders;

use App\Models\ManCompany;
use App\Models\ManCustomer;
use Illuminate\Database\Seeder;

class ManCustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ManCustomer::factory()->count(150)->create();
    }
}
