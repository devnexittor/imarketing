<?php

namespace Database\Seeders;

use App\Models\MlSummaryData;
use Illuminate\Database\Seeder;

class MlSummaryDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MlSummaryData::factory()->count(500)->create();
    }
}
