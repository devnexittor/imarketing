<?php

namespace Database\Seeders;

use App\Models\ManCompany;
use Illuminate\Database\Seeder;

class ManCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ManCompany::factory()->count(10)->create();
    }
}
