<?php

namespace App\Http\Controllers;

use App\Models\ManCompany;
use Illuminate\Http\Request;

class ManCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ManCompany  $manCompany
     * @return \Illuminate\Http\Response
     */
    public function show(ManCompany $manCompany)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ManCompany  $manCompany
     * @return \Illuminate\Http\Response
     */
    public function edit(ManCompany $manCompany)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ManCompany  $manCompany
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManCompany $manCompany)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ManCompany  $manCompany
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManCompany $manCompany)
    {
        //
    }
}
