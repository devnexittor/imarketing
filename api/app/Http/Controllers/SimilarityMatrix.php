<?php

namespace App\Http\Controllers;

use App\Models\ManCustomer;
use App\Models\ManPersonSale;
use App\Models\ManProduct;
use App\Models\MlSummaryData;
use Illuminate\Http\Request;

class SimilarityMatrix extends Controller
{
    // Procesar, comparar, aplicar el algoritmo de recomendación y generar una saida con una campaña personalizada
    public function GoSimilarityMatrix (Request $request) {
        $config = $request->all();
        $idCompany = $config["user_analytics"];

        // Consultamos todos los productos publicados
        $products = ManProduct::join('man_product_categories', 'fk_id_category', '=', 'id_category' )
            ->select('man_products.*')
            ->where('man_product_categories.user_analytics', $idCompany)
            ->where('man_product_categories.status', 1)
            ->where('man_products.status', 1)
            ->orderBy('man_products.id_product', 'ASC')
            ->get();

        // Consultamos la cartera de clientes de la empresa
        $clients = ManCustomer::join('man_person', 'fk_id_person', '=', 'id_person')
            ->where('user_analytics', $idCompany)
            ->where('man_person.status', 1)
            ->where('man_person.status', 1)
            ->where('man_person.email_confirmed', true)
            ->whereNotNull('man_person.email')
            ->orderBy('id_customer', 'ASC')
            ->get();

        $productProcess = [];
        foreach ($products as $product) {
            $productActually = [];
            foreach ($clients as $client) {
                // Consultamos si el usuario X compro el producto Y
                $clientShop = MlSummaryData::where('user_analytics', $idCompany)
                    ->where('fk_id_customer', $client->id_customer)
                    ->where('fk_id_product', $product->id_product)
                    ->where('acquired', 1)
                    ->first();
                if ($clientShop) {
                    array_push($productActually, 1); // Seteamos una compra
                } else {
                    array_push($productActually, 0); // Seteamos una NO compra
                }

            }
            // Agregamos el resumen del producto a la lista de productos
            array_push($productProcess, $productActually);
        }

        // Aplicamos la fórmula para la matriz de similitud
        $compatibility = [];
        $p = 0;
        foreach ($products as $product) {
            $p++;
            if (($p + 1) <= count($productProcess)) {
                for ($i = 1; $i <= count($productProcess); $i++) {
                    // Pasamos al script escrito en Python los vectores de comparación exect($productProcess[$p] , $productProcess[$i+$p]);
                    $k = $i + $p;
                    $command = escapeshellcmd("C:\laragon\www\imarketing\api\app\scriptPython\oparations.py " . json_encode($productProcess[$p]) . " " . json_encode($productProcess[$k]));
                    exec($command, $compare);
                    $compatibility[] = [
                        "id_product"    => $product->id_product,
                        "percentage"    => $compare
                    ];
                }
            }
        }

        // Generamos la recomendación del producto para cada uno de los clientes de la empresa
        foreach ($clients as $client) {
            // Verificamos si el usuario tiene una compra o una valoración
            $shopClient = ManPersonSale::where('fk_id_customer', $client->id_customer)->first();
            if ($shopClient) {
                $recommendation = [];
                // Buscamos las recomendaciones
                foreach ($compatibility as $item) {
                    if ($shopClient->fk_id_product === $item["id_product"]) {
                        array_push($recommendation, $item);
                    }
                }
                // Generamos una publicación personalizada para el cliente
                $this->generatePublish($client, $recommendation);
            } else {
                // Generarmos una publicación con un producto nuevo o en oferta
                $this->generatePublish($client);
            }
        }

    }

    private function RunSimilarityMatrix($config) {
        $dataSummary = MlSummaryData::where('user_analytics', $config['analytics'])->get();

        trader_mult();
    }

    private function generatePublish($client, array $recommendation = [])
    {
        if ($recommendation) {
            dd($recommendation);
        } else {
            dd($client);
        }
    }
}
