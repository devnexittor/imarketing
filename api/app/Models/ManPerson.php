<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManPerson extends Model
{
    use HasFactory;
    protected $table = "man_person";
    protected $primaryKey = "id_person";
    protected $fillable = [
        'user',
        'id_department',
        'device_id',
        'token',
        'wp_endpoint',
        'wp_auth',
        'wp_secret',
        'macaddr',
        'macaddr_bluetooth',
        'device_type',
        'name',
        'lastname',
        'cedula',
        'phone_country',
        'phone',
        'phone_home_country',
        'phone_home',
        'address',
        'latitude',
        'longitude',
        'email',
        'email_confirmed',
        'image_profile',
        'facebook_id',
        'facebook_picture',
        'instagram_id',
        'instagram_picture',
        'twitter_id',
        'webchat_id',
        'telegram_id',
        'password',
        'pin',
        'publicity',
        'birthday',
        'genre',
        'qualification',
        'registered_in',
    ];
}
