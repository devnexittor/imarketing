<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManProduct extends Model
{
    use HasFactory;
    protected $table = "man_products";
    protected $primaryKey = "id_product";
    protected $fillable = [
        'fk_id_category',
        'fk_id_product_currency',
        'name',
        'description',
        'detail',
        'reference',
        'price',
        'discount',
        'qr_code',
        'image',
        'image_1',
        'image_2',
        'image_3',
        'isnew',
        'ispromotion',
        'visits',
        'stock',
        'status',
    ];
}
