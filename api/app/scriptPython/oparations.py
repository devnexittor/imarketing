# importing required libraries
import numpy
import sys
print sys.argv[1]

# primer vector
v1 = numpy.array([0, 0, 1])
# segundo vector
v2 = numpy.array([0, 1, 1])

# multiplicamos los vectores
res_mul = v1 * v2

# Calculamos la dimensión de los vectores
res_dime = numpy.linalg.norm(v1) * numpy.linalg.norm(v2)

# RESPUESTA
response = (res_mul.sum() / res_dime)

print(response)
